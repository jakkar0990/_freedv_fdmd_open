-----------------------------------------------
FreeDV Project - Blu5 Mods
10:23 13/09/18
-----------------------------------------------
Hey There!

This project has been edited for the purposes of developing a data channel with the FreeDV API
and open-source code.

-------------------------------------------------------------------------------
Using Mods
-------------------------------------------------------------------------------

----------------------
Prerequisites
----------------------

For the project to work with the mods need to be careful of three things:

- The code here was designed with the FreeDV 1.2.2 project
- The codec2 library that freedv uses in the build_linux directory
- The FreeDV source code that actually builds the app


----------------------
Install Guide
----------------------
Firstly you should get a default version of the FreeDV.1.2.2 package

Then just do the default build process:
- mkdir build_linux
- cd build_linux
- cmake ..
- make 

Hopefully that works!

Then what you do is:
 - Replace the standard codec2 library contents with those of the modified Codec2 Library
 - Replace the Source Code of the FreeDV app with the modified ones

Then run:
- make

The build should run successfully! Then ofcourse whether the app runs as desired depends on the mod version!

--------------------------------------------------------------------------------
Codec2 Source Code
---------------------------------------------------------------------------------

The Codec2 Library is found here: /home/jak/FreeDV_DEV/build_linux/codec2-prefix/src/codec2

The FreeDV Source code is: /home/jak/FreeDV_DEV/src

So you know FreeDV_DEV is the folder for the data. 

---------------------------------------------------------------------------------






